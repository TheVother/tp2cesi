# Partie 1 : Mise en place de la solution

**Z'est bardi z'est bardiii.**

## Sommaire

- [Partie 1 : Mise en place de la solution](#partie-1--mise-en-place-de-la-solution)
  - [Sommaire](#sommaire)
- [I. Setup base de données](#i-setup-base-de-données)
  - [1. Install MariaDB](#1-install-mariadb)
  - [2. Conf MariaDB](#2-conf-mariadb)
  - [3. Test](#3-test)
- [II. Setup Apache](#ii-setup-apache)
  - [1. Install Apache](#1-install-apache)
    - [A. Apache](#a-apache)
    - [B. PHP](#b-php)
  - [2. Conf Apache](#2-conf-apache)
- [III. NextCloud](#iii-nextcloud)
  - [4. Test](#4-test)

# I. Setup base de données

## 1. Install MariaDB

🌞 **Installer MariaDB sur la machine `db.tp2.cesi`**

```bash
[maxence@node1 ~]$ sudo dnf install mariadb-server
[sudo] password for maxence:
Last metadata expiration check: 23:06:52 ago on Mon 06 Dec 2021 02:55:33 PM CET.
Package mariadb-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

🌞 **Le service MariaDB**

```bash
[maxence@node1 ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.

[maxence@node1 ~]$ sudo systemctl start mariadb.service
[maxence@node1 ~]$ sudo systemctl status mariadb.service
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 14:05:01 CET; 2s ago

sudo ss -lutpn  | grep "mysql"
tcp   LISTEN 0      80                 *:3306            *:*    users:(("mysqld",pid=2769,fd=21))

[maxence@node1 ~]$ sudo ps -q 2769
    PID TTY          TIME CMD
   2769 ?        00:00:00 mysqld

[maxence@node1 ~]$ sudo ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
mysql       2769       1  0 14:05 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr


```

🌞 **Firewall**

```bash
[maxence@node1 ~]$ sudo firewall-cmd --permanent --add-port=3306/tcp
success
[maxence@node1 ~]$ sudo firewall-cmd --reload
success
```

## 2. Conf MariaDB

Première étape : le `mysql_secure_installation`. C'est un binaire (= une commande, une application, un programme, ces mots désignent la même chose) qui sert à effectuer des configurations très récurrentes, on fait ça sur toutes les bases de données à l'install.  
C'est une question de sécu.

🌞 **Configuration élémentaire de la base**


```bash
[maxence@node1 ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.


Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

You already have a root password set, so you can safely answer 'n'.

Change the root password? [Y/n] n
 ... skipping.

By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

🌞 **Préparation de la base en vue de l'utilisation par NextCloud**


```bash
[maxence@node1 ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 21
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```


```bash

MariaDB [(none)]> CREATE USER 'nextcloud'@'192.168.56.4' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'192.168.56.4';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)

```

## 3. Test

➜ **On va tester que la base sera utilisable par NextCloud.**

Concrètement il va faire quoi NextCloud vis-à-vis de la base MariaDB ?

- se connecter sur le port où écoute MariaDB
- la connexion viendra de `web.tp2.cesi`
- il se connectera en utilisant l'utilisateur `nextcloud`
- il écrira/lira des données dans la base `nextcloud`

➜ Il faudrait donc qu'on teste ça, à la main, **depuis la machine `web.tp2.cesi`**.

Bah c'est parti ! Il nous faut juste un client pour nous connecter à la base depuis la ligne du commande : il existe une commande `mysql` pour ça.

🌞 **Installez sur la machine `web.tp2.cesi` la commande `mysql`**

```bash
[maxence@web ~]$ sudo dnf provides mysql
[sudo] password for maxence:
Last metadata expiration check: 1 day, 0:08:11 ago on Mon 06 Dec 2021 02:55:33 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

[maxence@web ~]$sudo dnf install mysql
Installed:
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch               mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
  mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Complete!
[maxence@web ~]$
```


🌞 **Tester la connexion**


```bash
[maxence@web ~]$ sudo mysql -u nextcloud -p  -h 192.168.56.3 nextcloud
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 43
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> SHOW TABLES;
Empty set (0.00 sec)
```

C'est bon ! Ca tourne ! **On part sur Apache maintenant !**

# II. Setup Apache

> La section II et III sont clairement inspirés de [la doc officielle de Rocky pour installer NextCloud](https://docs.rockylinux.org/guides/cms/cloud_server_using_nextcloud/).

Comme annoncé dans l'intro, on va se servir d'Apache dans le rôle de serveur Web dans ce TP5. Histoire de varier les plaisirs è_é

![Linux is a tipi](./pics/linux_is_a_tipi.jpg)

## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine `web.tp2.cesi`**

```bash
[maxence@web ~]$ sudo dnf install httpd
Installed:
  apr-1.6.3-12.el8.x86_64
  apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64
  apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch
  httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64
  rocky-logos-httpd-85.0-3.el8.noarch

Complete!
```

🌞 **Analyse du service Apache**


```bash
[maxence@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

[maxence@web ~]$ sudo systemctl start httpd

[maxence@web ~]$ sudo ss -lutpn | grep "httpd"
tcp   LISTEN 0      128                *:80              *:*    users:(("httpd",pid=2395,fd=4),("httpd",pid=2394,fd=4),("httpd",pid=2393,fd=4),("httpd",pid=2387,fd=4))

[maxence@web ~]$ sudo ps -ef
apache      2392    2387  0 15:42 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2393    2387  0 15:42 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2394    2387  0 15:42 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2395    2387  0 15:42 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```
- le service aussi s'appelle `httpd`
- lancez le service `httpd` et activez le au démarrage
- isolez les processus liés au service `httpd`
- déterminez sur quel port écoute Apache par défaut
- déterminez sous quel utilisateur sont lancés les processus Apache

---

🌞 **Un premier test**

```bash
[maxence@web ~]$ sudo firewall-cmd --permanent --add-port=443/tcp
success
[maxence@web ~]$ sudo firewall-cmd --reload
success

PS C:\Users\a.lemoine> curl 192.168.56.4
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system.
If you can read this page, it means that the software it working correctly.
Just visiting?
This website you are visiting is either experiencing problems or could be going through maintenance.
If you would like the let the administrators of this website know that you've seen this page instead of the page
you've expected, you should send them an email. In general, mail sent to the name "webmaster" and directed to the
website's domain should reach the appropriate person.
The most common email address to send to is: "webmaster@example.com"
Note:
[.....]
```

- ouvrez le port d'Apache dans le firewall
- testez, depuis votre PC, que vous pouvez accéder à la page d'accueil par défaut d'Apache
  - avec une commande `curl`
  - avec votre navigateur Web

### B. PHP

NextCloud a besoin d'une version bien spécifique de PHP.  
Suivez **scrupuleusement** les instructions qui suivent pour l'installer.

🌞 **Installer PHP**

```bash
[maxence@web ~]$ sudo dnf install epel-release
Complete!
[maxence@web ~]$sudo dnf update
Complete!
[maxence@web ~]$sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Complete!
[maxence@web ~]$sudo dnf module enable php:remi-7.4
Complete!
[maxence@web ~]$sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php- [........]
Complete!
```

## 2. Conf Apache


🌞 **Analyser la conf Apache**

```bash
[maxence@web ~]$sudo nano /etc/httpd/conf/httpd.conf ==> IncludeOptional conf.d/*.conf
```

🌞 **Créer un VirtualHost qui accueillera NextCloud**


```bash
[maxence@web ~]$sudo touch /etc/httpd/conf.d/vhosts.conf

[maxence@web ~]$ls /etc/httpd/conf.d/vhosts.conf ==> /etc/httpd/conf.d/vhosts.conf

<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/  

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi  

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

> N'oubliez pas de redémarrer le service à chaque changement de la configuration, pour que les changements prennent effet.

🌞 **Configurer la racine web**

```bash
[maxence@web ~]$sudo mkdir /var/www/nextcloud/
[maxence@web ~]$sudo mkdir /var/www/nextcloud/html/
[maxence@web ~]$sudo chown apache /var/www/nextcloud/html/

[maxence@web ~]$sudo ls -la /var/www/nextcloud/html
drwxr-xr-x. 3 apache root 8 Dec 09:33 .
drwxr-xr-x. 2 apache root 8 Dec 09:33 html
```

🌞 **Configurer PHP**


```bash
[maxence@web ~]$sudo nano /etc/opt/ remi/php74/php.ini

date.timezone = "Europe/Paris"
```

# III. NextCloud

On dit "installer NextCloud" mais en fait c'est juste récupérer les fichiers PHP, HTML, JS, etc... qui constituent NextCloud, et les mettre dans le dossier de la racine web.

🌞 **Récupérer Nextcloud**

```bash
[maxence@web ~]$sudo curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
[maxence@web ~]$sudo ls 
nextcloud-21.0.1.zip
```

🌞 **Ranger la chambre**

```bash
[maxence@web ~]$sudo unzip nextcloud-21.0.1.zip
[maxence@web ~]$sudo ls
nextcloud-21.0.1.zip nextcloud

[maxence@web ~]$sudo rm -f nextcloud-21.0.1.zip

[maxence@web ~]$sudo mv /home/sancur/nextcloud /var/www/nextcloud/html/

[maxence@web ~]$sudo ls /var/www/nextcloud/html/
nextcloud

[maxence@web ~]$sudo chown apache -R /var/www/nextcloud/html/nextcloud/

[maxence@web ~]$sudo ls -la 
[ tous les fichiers appartiennent au user apache....] 

```

## 4. Test


🌞 **Modifiez le fichier `hosts` de votre PC**

```bash
Editez le fichier hosts et rajouter : 
192.168.56.4 web.tp2.cesi
```

🌞 **Tester l'accès à NextCloud et finaliser son install'**

```bash
Se rendre sur la page http://web.tp2.cesi/nextcloud/index.php

Créer l'admin : maxence + Mdp

Répertoire BDD : 

/var/www/nextcloud/html/data

Choisir mariadb comme BDD : 

nextcloud : User
Mdp : meow
BDD : nextcloud
IP : 192.168.56.3:3306

Je peux me connecter sur NextCLoud avec le compte créé
```

**🔥🔥🔥 Baboom ! Un beau NextCloud.**


# Partie 2 : Sécurisation

## Sommaire

- [Partie 2 : Sécurisation](#partie-2--sécurisation)
  - [Sommaire](#sommaire)
- [I. Serveur SSH](#i-serveur-ssh)
  - [1. Conf SSH](#1-conf-ssh)
  - [2. Bonus : Fail2Ban](#2-bonus--fail2ban)
- [II. Serveur Web](#ii-serveur-web)
  - [1. Reverse Proxy](#1-reverse-proxy)
  - [2. HTTPS](#2-https)

# I. Serveur SSH

## 1. Conf SSH

🌞 **Modifier la conf du serveur SSH**

```bash
Sur machine physique : ssh-keygen -t ed25519

Ensuite copier la clé vers le serveur Apache depuis le client :

cat .ssh/id_rsa.pub | ssh maxence@192.168.56.4 'cat >> .ssh/authorized_keys'

CHanger les permissions de .ssh :

chmod 640 -R .ssh/

Désactivation de root : 

[maxence@web ~]$sudo nano /etc/ssh/sshd_config

PermitRootLogin no
PasswordAuthentification no
```

# II. Serveur Web


## 1. Reverse Proxy


🖥️ **Créez une nouvelle machine : `proxy.tp2.cesi`.** 🖥️

🌞 **Installer NGINX**

```bash
[maxence@proxy ~]$sudo dnf install nginx
Complete!
```
🌞 **Configurer NGINX comme reverse proxy**

```bash
[maxence@poxy ~]$sudo nano /etc/nginx/conf.d/proxy.conf

server {
    listen 80;
    server_name proxy.tp2.cesi;

    location /{
       proxy_pass http://web.tp2.cesi/nextcloud/index.php;
    }
}
```

🌞 **Une fois en place, text !**
```bash
Editez le fichier vhosts
10.2.1.13 web.tp2.cesi
```
## 2. HTTPS

🌞 **Générer une clé et un certificat avec la commande suivante :**

```bash
[maxence@proxy ~]$sudo openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt


server.crt  server.key
```


🌞 **Allez, faut ranger la chambre**

```bash
[maxence@proxy ~]$sudo cp server.crt /etc/pki/tls/certs/web.tp2.cesi.crt
[maxence@proxy ~]$sudo cp server.key /etc/pki/tls/private/web.tp2.cesi.key
```

🌞 **Affiner la conf de NGINX**

```bash
[maxence@proxy ~]$sudo nano /etc/nginx/conf.d/proxy.conf
# cette ligne 'listen', vous l'avez déjà. Remplacez-la.
listen                  443 ssl http2;

# nouvelles lignes
# remplacez les chemins par la clé et le cert que vous venez de générer
ssl_certificate         /etc/pki/tls/certs/web.tp2.cesi.crt;
ssl_certificate_key     /etc/pki/tls/private/web.tp2.cesi.key;



[maxence@proxy ~]$sudo firewall-cmd --add-port=443/tcp --permanent
success! 
[maxence@proxy ~]$sudo firewall-cmd --reload
success!             
``` 

# Partie 3 : Maintien en condition opérationnelle

## Sommaire

- [Partie 3 : Maintien en condition opérationnelle](#partie-3--maintien-en-condition-opérationnelle)
  - [Sommaire](#sommaire)
- [I. Monitoring](#i-monitoring)
  - [1. Intro](#1-intro)
  - [2. Setup Netdata](#2-setup-netdata)
  - [3. Bonus : alerting](#3-bonus--alerting)
  - [4. Bonus : proxying](#4-bonus--proxying)
- [II. Backup](#ii-backup)

# I. Monitoring

## 2. Setup Netdata


🌞 **Installez Netdata** en exécutant la commande suivante :

```bash
[maxence@proxy ~]$sudo bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
```
🌞 **Démarrez Netdata**


```bash
[maxence@proxy ~]$sudo systmctl start netdata
[maxence@proxy ~]$sudo systmctl enable netdata
[maxence@proxy ~]$sudo firewall-cmd --add-port=19999/tcp --permanent
success
[maxence@proxy ~]$sudo firewall-cmd --reload
success
```

# II. Backup

🌞 **Téléchargez Borg** sur la machine `web.tp2.cesi`

```bash
[maxence@web ~]$sudo curl -SLO https://github.com/borgbackup/borg/releases/download/1.1.17/borg-linux64
[maxence@web ~]$sudo cp borg-linux64 /usr/local/bin/borg
[maxence@web ~]$sudo chown root:root /usr/local/bin/borg
[maxence@web ~]$sudo chmod 755 /usr/local/bin/borg
```

🌞 **Jouer avec Borg**

```bash
[maxence@web ~]$sudo borg init --encryption=repokey /srv/backup
[maxence@web ~]$sudo borg create /srv/backup::Monday ~/home/rockycesi/Documents

```
🌞 **Ecrire un script**

```bash

sudo nano /home/maxence/script.sh

#!/bin/sh

/usr/bin/borg create /srv/backup/nextcloud::nextcloud_211208_163815 ~/var/www/nextcloud/

```

🌞 **Créer un service**

```bash
[maxence@web ~]$sudo nano /etx/systemd/system/backup_db.service

[Unit]
Description=<DESCRIPTION>

[Service]
ExecStart=<COMMAND>
Type=oneshot

[Install]
WantedBy=multi-user.target
```

🌞 **Créer un timer**

```bash

[maxence@web ~]$sudo nano /etc/systemd/system/backup.timer

[Unit]
Description=Lance backup.service à intervalles réguliers
Requires=backup.service

[Timer]
Unit=backup.service
OnCalendar=hourly

[Install]
WantedBy=timers.target
```


Activez maintenant le *timer* avec :

```bash
[maxence@web ~]$sudo systemctl daemon-reload
[maxence@web ~]$sudo systemctl start backup.timer
[maxence@web ~]$sudo systemctl enable backup.timer
```

🌞 **Vérifier que le *timer* a été pris en compte**, en affichant l'heure de sa prochaine exécution :

```bash
[maxence@web ~]$sudo systemctl list-timers
NEXT			    LEFT
Wed 2021-12-08 17:40:45 CET 59min left 



